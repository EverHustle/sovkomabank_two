package com.falcode.sovkombank_two;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private AlertDialog.Builder adb;

    private LinearLayout inScrollLL;
    private View itemToRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.inScrollLL = (LinearLayout) findViewById(R.id.inscroll_ll);

        this.fillListWithItems(20);
    }

    // dynamically add items to the layout
    private void fillListWithItems(int itemsAmount) {
        for (int i = 1; i <= itemsAmount; i++)
            this.addItemToList("Inscroll item " + i, i);
    }

    // dynamically adds parameters to addd items
    private void addItemToList(String text, int id) {
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout newItemLL = (LinearLayout) inflater.inflate(R.layout.scroll_view_item, null);

        newItemLL.setTag(R.id.item_tv + "_" + id);
        TextView newItemTV = (TextView) newItemLL.findViewById(R.id.item_tv);
        newItemTV.setText(text);

        this.inScrollLL.addView(newItemLL);
    }

    // onClick function for added item -> calls a dialog
    public void scrollViewItemOnClick(View item) {
        this.itemToRemove = item;
        this.adb = new AlertDialog.Builder(this);
        this.adb.setTitle("Confirm action");
        this.adb.setIcon(R.mipmap.ic_launcher);
        this.adb.setPositiveButton("Yes", this.dialogButtonsListener);
        this.adb.setNegativeButton("No", this.dialogButtonsListener);

        TextView inItemTV = (TextView) MainActivity.this
                .itemToRemove.findViewById(R.id.item_tv);
        this.adb.setMessage("Do you really want to remove item '" + inItemTV.getText() + "'");
        this.adb.create();
        this.adb.show();
    }

    // dialog builder
    private DialogInterface.OnClickListener dialogButtonsListener =
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case android.app.Dialog.BUTTON_POSITIVE:
                            if (MainActivity.this.itemToRemove != null) {
                                LinearLayout parent =
                                        (LinearLayout) MainActivity.this.itemToRemove.getParent();
                                parent.removeView(MainActivity.this.itemToRemove);
                            }
                            break;
                        case android.app.Dialog.BUTTON_NEGATIVE:
                            MainActivity.this.itemToRemove = null;
                            break;
                    }
                }
            };
}
